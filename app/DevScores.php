<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevScores extends Model
{
    protected $table = 'devscore';
}
