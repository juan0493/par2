<?php

namespace App\Exports;

use App\Subs;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
  /**
  * @return \Illuminate\Support\Collection
  */
  public function collection()
  {
    $options = [
      'sv' => 'El Salvador',
      'gt' => 'Guatemala',
      'cr' => 'Costa Rica',
      'nc' => 'Nicaragua',
      'hn' => 'Honduras',
    ];
    $resp = Subs::select('fname', 'lname', 'phone', 'email', 'country', 'created_at')->get();

    foreach ($resp as $value) {
      $value->country = isset($options[$value->country]) ? $options[$value->country] : 'Desconocido';
    }
    return $resp;
  }

  public function headings(): array
  {
    return [
      'Nombre',
      'Apellido',
      'Teléfono',
      'Correo',
      'Fecha de inscripción',
    ];
  }

  public function registerEvents(): array
  {
    return [
      AfterSheet::class    => function(AfterSheet $event) {
        $cellRange = 'A1:Z1'; // All headers
        $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
        $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
      },
    ];
  }
}
