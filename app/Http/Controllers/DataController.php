<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;
use App\Town;

class DataController extends Controller
{
  public function getTowns ($dartmentId) {
    return Town::where('department_id','=',$dartmentId)->get();
  }
  
  public function getStores ($TownId) {
    return Store::where('town_id','=',$TownId)->get();
  }

}
