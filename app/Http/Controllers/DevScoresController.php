<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\DevScores;
use App\DevSubs;

class DevScoresController extends Controller
{
  private $pass = '$2y$10$fhmSDLMTvmWX83J03bjg.uQzmm0zrZ6o92zVtx5Hz.2wfEJQ/WdOK';
  private $msgSuccess = 'success';
  private $noData = 'Missing data';
  private $msgError = 'Wrong password';
  private $mail = 'email';

  public function save(Request $request) {
    if(!isset($request->clave) || !isset($request->email) || !isset($request->score)) {
      return array('msg' => $this->noData);
    }
    if (Hash::check($request->clave, $this->pass)) {

      $user = DevScores::where('name', '=', $request->email)->first();
      $msg = '';
      if( $user ) {
        if ($request->score > $user->score) {
          $user->score = $request->score;
          $user->save();
        }
        $msg = $this->msgSuccess;
      } else {
        $msg = 'Username doesnt exist';
      }
      
      return array('msg' => $msg);
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function positions() {
    return DevScores::orderBy('score', 'desc')->get();
  }

  public function reset(Request $request) {
    if(!isset($request->clave) || !isset($request->email)) {
      return array('msg' => $this->noData);
    }

    if (Hash::check($request->clave, $this->pass)) {
      $user = DevScores::where('name', '=', $request->email)->first();
      $resp = null;
      if( $user ) {
        $user->score = 0;
        $user->save();
        $resp = array('msg' => $this->msgSuccess);
      } else {
        $resp = array('msg' => 'Username doesnt exist');
      }

      return $resp;
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function register(Request $request) {
    if(
      !isset($request->clave) ||
      !isset($request->name) ||
      !isset($request->lname) ||
      !isset($request->email)
    ) {
      return array('msg' => $this->noData);
    }
    if (Hash::check($request->clave, $this->pass)) {

      $user = DevSubs::where($this->mail, '=', $request->email)->first();
      if( !$user ) {
        $newuser = new DevSubs;
        $newuser->fname = $request->name;
        $newuser->lname = $request->lname;
        $newuser->email = $request->email;
        $newuser->phone = 'apk';
        $newuser->save();

        $userScore = new DevScores;
        $userScore->name = $request->email;
        $userScore->score = 0;
        $userScore->save();
      }
      return array('msg' => $this->msgSuccess);
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function check(Request $request) {
    $user = DevSubs::where($this->mail, '=', $request->email)->first();
    if ($user) {
      return array('msg' => true);
    }
    return array('msg' => false);
  }
  public function remove(Request $request) {
    if(!isset($request->clave) || !isset($request->email)) {
      return array('msg' => $this->noData);
    }
    if (Hash::check($request->clave, $this->pass)) {
      $resp = null;
      $user = DevSubs::where($this->mail, '=', $request->email)->first();
      $userScore = DevScores::where('name', '=', $request->email)->first();
      if( $user && $userScore ) {        
        $resp = $user->delete() && $userScore->delete() ? $this->msgSuccess : 'Error deleting';
      } else {
        $resp = 'Wrong email';
      }
      return array('msg' => $resp);
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function all() {
    return DevSubs::All();
  }
}
