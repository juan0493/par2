<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Country;
use App\Department;
use App\Subs;
use App\Catalog;
use App\Scores;

class FlipbookController extends Controller
{
  public function index ($country) {
    $country = strtolower($country);
    $catalogue = Catalog::where('code' ,'=', $country)->first();
    if(!$catalogue) {
      return redirect('/');
    }

    $data['country'] = $catalogue->name;
    $data['link'] = $catalogue->link;
    $data['comp'] = $country;
    $data['banner'] = [
      'sv' => 5,
      'gt' => 5,
      'cr' => 5,
      'nc' => 5,
      'hn' => 5,
    ];

    $dbCountry = Country::where('name','=',$data['country'])->first();
    $data['dbDepartment'] = Department::where('country_id','=',$dbCountry->id)->orderBy('name','asc')->get();
    $data['leader'] = Scores::orderBy('score', 'desc')->take(10)->get();
    return view('flip', $data);
  }

  public function saveUser (Request $request) {
    $valid = Subs::where('email','=',$request->input('email'))->first();
    if ($valid) {
      return 'duplicated';
    }
    $newUser = new Subs;
    $newUser->fname = $request->input('fname');
    $newUser->lname = $request->input('lname');
    $newUser->phone = $request->input('phone');
    $newUser->email = $request->input('email');
    $newUser->country = $request->input('country');

    $newUser->save();
    return 'success';
  }

  public function subs(Request $request) {
    $pass = '$2y$10$fhmSDLMTvmWX83J03bjg.uQzmm0zrZ6o92zVtx5Hz.2wfEJQ/WdOK';
    
    if (!isset($request->clave)) {
      return redirect('/preview/temporal-view/subs');
    }

    if (Hash::check($request->clave, $pass)) {
      $data['subs'] = Subs::All();
      return view('tmp', $data);
    } else {
      $data['msg'] = 'Clave incorrecta';
      return view('tmp-home', $data);
    }
    
  }  
}
