<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Scores;
use App\Subs;

class ScoresController extends Controller
{
  private $pass = '$2y$10$fhmSDLMTvmWX83J03bjg.uQzmm0zrZ6o92zVtx5Hz.2wfEJQ/WdOK';
  private $msgSuccess = 'success';
  private $noData = 'Missing data';
  private $msgError = 'Wrong password';
  private $mail = 'email';

  public function save(Request $request) {
    if(!isset($request->clave) || !isset($request->name) || !isset($request->score)) {
      return array('msg' => $this->noData);
    }
    if (Hash::check($request->clave, $this->pass)) {

      $user = Scores::where('name', '=', $request->name)->first();
      if( $user ) {
        if ($request->score > $user->score) {
          $user->score = $request->score;          
        }
      } else {
        $user = new Scores;
        $user->name = $request->name;
        $user->score = $request->score;
      }
      $user->save();
      return array('msg' => $this->msgSuccess);
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function positions() {
    return Scores::orderBy('score', 'desc')->get();
  }

  public function reset(Request $request) {
    if(!isset($request->clave) || !isset($request->name)) {
      return array('msg' => $this->noData);
    }

    if (Hash::check($request->clave, $this->pass)) {
      $user = Scores::where('name', '=', $request->name)->first();
      $resp = null;
      if( $user ) {
        $user->score = 0;
        $user->save();
        $resp = array('msg' => $this->msgSuccess);
      } else {
        $resp = array('msg' => 'Username doesnt exist');
      }

      return $resp;
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function register(Request $request) {
    if(
      !isset($request->clave) ||
      !isset($request->name) ||
      !isset($request->lname) ||
      !isset($request->email)
    ) {
      return array('msg' => $this->noData);
    }
    if (Hash::check($request->clave, $this->pass)) {

      $user = Subs::where($this->mail, '=', $request->email)->first();
      if( !$user ) {
        $newuser = new Subs;
        $newuser->fname = $request->fname;
        $newuser->lname = $request->lname;
        $newuser->email = $request->email;
        $newuser->save();
      }
      return array('msg' => $this->msgSuccess);
    } else {
      return array('msg' => $this->msgError);
    }
  }

  public function check(Request $request) {
    $user = Subs::where($this->mail, '=', $request->email)->first();
    if ($user) {
      return true;
    }
    return false;
  }
  public function remove(Request $request) {
    if(!isset($request->clave) || !isset($request->email)) {
      return array('msg' => $this->noData);
    }
    if (Hash::check($request->clave, $this->pass)) {
      $resp = null;
      $user = Subs::where($this->mail, '=', $request->email)->first();
      if( $user ) {
        $resp = $user->delete() ? $this->msgSuccess : 'Error deleting';
      } else {
        $resp = 'Wrong email';
      }
      return array('msg' => $resp);
    } else {
      return array('msg' => $this->msgError);
    }
  }
}
