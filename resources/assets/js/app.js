import axios from 'axios';
import swal from 'sweetalert';
import validator from 'email-validator';
import Swiper from 'swiper';
import format from 'format-number';
import yo from 'yo-yo';
require('smoothscroll-polyfill').polyfill();

const calcComponent = document.querySelector('.calc-component');
let range = null;
let active = true;
let instance, type;
let total = 0.00;
let wow = 0.00;
let sending = false; // Prevent double request

const clearForm = () => {
  document.getElementById('fname').value = '';
  document.getElementById('lname').value = '';
  document.getElementById('phone').value = '';
  document.getElementById('email').value = '';
  instance.close();
};

const sendData = (ev) => {
  ev.preventDefault();
  if (sending) {
    return false;
  }
  sending = true;
  const fname = document.getElementById('fname').value || null;
  const lname = document.getElementById('lname').value || null;
  const phone = document.getElementById('phone').value || null;
  const email = document.getElementById('email').value || null;
  const country = localStorage.getItem('currentCountry') || null;

  if (fname && lname && phone && email) {

    if (!validator.validate(email)) {
      sending = false;
      swal('Advertencia', 'Correo inválido', 'warning');
      return false;
    }

    axios.post('/saveUser', {
      fname,
      lname,
      phone,
      email,
      country,
    })
      .then((response) => {
        if (response.data === 'duplicated') {
          swal('Error', 'Este correo ya está registrado', 'error');
        } else {
          swal('¡Listo!', 'Ya estás suscrito a nuestras promociones', 'success');
          clearForm();
        }
      })
      .catch((error) => {
        swal('Error', 'No se pudo completar la acción, intenta más tarde', 'error');
      })
      .then(() => {
        sending = false;
      });
  } else {
    sending = false;
    swal('Advertencia', 'Completa los campos antes de enviar', 'warning');
  }
};

const toFixed = (value, decimals) => {
  return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

const addAmount = (ev) => {
  const showWow = calcComponent.querySelector('.top').querySelector('.number').querySelector('span');
  const showTotal = calcComponent.querySelector('.top').querySelector('.calc-total');
  const price = ev.target.getAttribute('data-amount');
  if (active) {
    total += parseFloat(price);
    total = toFixed(total, 2);
    active = false;
    console.log(total);
    updateTotal();
    Equal(true);
  }
};

const Equal = (bt) => {
  if (bt != true) {
    total = 0;
    active = true;
    updateTotal();
  }
  for (let value of range) {
    if (total >= parseFloat(value.min) && total <= parseFloat(value.max)) {
      wow = parseFloat(value.wow);
      updateWow();
      return false;
    }
  }
};

const updateTotal = () => {
  const showTotal = calcComponent.querySelector('.top').querySelector('.calc-total');
  const formatTotal = format({
    integerSeparator: ',',
    truncate: 2,
  })(total);
  showTotal.textContent = `${type}${formatTotal}`;
};

const updateWow = () => {
  const showWow = calcComponent.querySelector('.top').querySelector('.number').querySelector('span');
  const formatWow = format({ integerSeparator: ',' })(wow);
  showWow.innerHTML = `<sup>${type}</sup>${formatWow}`;
};

const activatePlus = () => { active = true; };

const cleanCalc = () => {
  wow = 0.00;
  total = 0.00;
  active = true;
  updateTotal();
  updateWow();
};

// Calc
const calcFunction = (data) => {
  const buttons = calcComponent.querySelector('.cont-butons').querySelectorAll('.n');
  calcComponent.querySelector('.cont-butons').querySelector('.pl').addEventListener('click', activatePlus);
  calcComponent.querySelector('.cont-butons').querySelector('.cl').addEventListener('click', cleanCalc);
  calcComponent.querySelector('.cont-butons').querySelector('.eq').addEventListener('click', Equal);

  type = data.type;
  range = data.range;
  buttons.forEach((el, index) => {
    el.addEventListener('click', addAmount);
    el.setAttribute('data-amount', data[index + 1]);
  });
};

// navigate
document.getElementById('li-game').addEventListener('click', ev => document.querySelector('#leaderboard-cont').scrollIntoView({ behavior: 'smooth' }));
document.getElementById('li-catalogue').addEventListener('click', ev => document.querySelector('#catalogo').scrollIntoView({ behavior: 'smooth' }));
document.getElementById('li-promo').addEventListener('click', ev => document.querySelector('#promociones').scrollIntoView({ behavior: 'smooth' }));
document.getElementById('li-store').addEventListener('click', ev => document.querySelector('#store').scrollIntoView({ behavior: 'smooth' }));
document.getElementById('go-home').addEventListener('click', ev => document.querySelector('header').scrollIntoView({ behavior: 'smooth' }));

// Waypoint
new Waypoint({
  element: document.querySelector('.video-container'),
  handler: (direction) => {
    document.getElementById('go-home').classList.remove('hide');
  }
});
new Waypoint({
  element: document.querySelector('#li-promo'),
  handler: (direction) => {
    document.getElementById('go-home').classList.add('hide');
  }
});

// Store functions
const hideProgress = () => {
  document.querySelector('.progress').classList.add('op');
};

const showProgress = () => {
  document.querySelector('.progress').classList.remove('op');
};

const getCity = (ev) => {
  ev.preventDefault();
  showProgress();
  const value = ev.target.value;
  const City = document.querySelector('#selectCity');
  City.disabled = true;

  axios.get(`/api/getTowns/${value}`)
    .then((response) => {
      for (var i = City.options.length - 1; i > 0; i--) {  // Remove all options except first
        City.removeChild(City.options[i]);
      }
      response.data.forEach((el) => {
        const option = document.createElement('option');
        option.text = el.name;
        option.value = el.id;
        City.add(option);
      });
      City.selectedIndex = 0;
      City.disabled = City.options.length > 1 ? false : true;
      M.FormSelect.init(City);
    }).then(() => {
      hideProgress();
    });
};

const getStores = (ev) => {
  ev.preventDefault();
  showProgress();
  const container = document.getElementById('result-stores');
  const value = ev.target.value;
  const name = ev.target.options[ev.target.selectedIndex].innerHTML;

  axios.get(`/api/getStores/${value}`)
    .then((response) => {
      container.innerHTML = '';

      const title = yo`<div class="col s12"><h5>Tiendas en el municipio de ${name}</h5></div>`;
      container.appendChild(title);

      response.data.forEach((el) => {
        const newStore = yo`<div class="col s12 m6">
          <div class="card yellow darken-4">
            <div class="card-content white-text">
              <span class="card-title">Teléfono: ${el.phone}</span>
              <p>${el.address}</p>
            </div>
            <div class="card-action">
              <a target="_blank" href="https://www.google.com/maps/search/?api=1&query=${el.latitude},${el.longitude}"><img src='/img/v2/gm.png' title="Ir con Google Maps" /></a>
              <a target="_blank" href="https://www.waze.com/ul?ll=${el.latitude}%2C${el.longitude}&navigate=yes&zoom=17"><img src='/img/v2/waze.png' title="Ir con Waze" /></a>
            </div>
          </div>
        </div>`;
        container.appendChild(newStore);
      });
    }).then(() => {
      hideProgress();
    });
};
document.getElementById('selectCity').addEventListener('change', getStores);
document.getElementById('selectDpto').addEventListener('change', getCity);

// Init Page
document.addEventListener('DOMContentLoaded', () => {
  const elems = document.querySelectorAll('.modal');
  M.Modal.init(elems);
  instance = M.Modal.getInstance(document.querySelector('.modal'));

  new Swiper('.swiper-container', {
    autoplay: {
      delay: 5000,
    },
    pagination: {
      el: '.swiper-pagination',
    },
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  const imgPromos = document.querySelectorAll('.materialboxed');
  M.Materialbox.init(imgPromos);

  const dropElements = document.querySelectorAll('select');
  M.FormSelect.init(dropElements);

  setTimeout(() => {
    instance.open();
  }, 20000);

  document.getElementById('sendData').addEventListener('click', sendData);
  // const currentCountry = calcComponent.querySelector('.hide').textContent;
  // axios.get(`/calc/${currentCountry}.json`).then(response => calcFunction(response.data));
});