import axios from 'axios';
import swal from 'sweetalert';
import validator from 'email-validator';

let instance;

const clearForm = () => {
  document.getElementById('fname').value = '';
  document.getElementById('lname').value = '';
  document.getElementById('phone').value = '';
  document.getElementById('email').value = '';
  instance.close();
};

const sendData = (ev) => {
  ev.preventDefault();
  const fname = document.getElementById('fname').value || null;
  const lname = document.getElementById('lname').value || null;
  const phone = document.getElementById('phone').value || null;
  const email = document.getElementById('email').value || null;



  if (fname && lname && phone && email) {

    if (!validator.validate(email)) {
      swal('Advertencia', 'Correo inválido', 'warning');
      return false;
    }

    axios.post('/saveUser', {
      fname,
      lname,
      phone,
      email,
    })
      .then((response) => {
        if (response.data === 'duplicated') {
          swal('Error', 'Este correo ya está registrado', 'error');
        } else {
          swal('¡Listo!', 'Ya estás suscrito a nuestras promociones', 'success');
          clearForm();
        }
      })
      .catch((error) => {
        swal('Error', 'No se pudo completar la acción, intenta más tarde', 'error');
      });
  } else {
    swal('Advertencia', 'Completa los campos antes de enviar', 'warning');
  }
};

document.addEventListener('DOMContentLoaded', () => {
  const info = document.getElementById('info');
  const elems = document.querySelectorAll('.modal');
  const instances = M.Modal.init(elems);
  instance = M.Modal.getInstance(document.querySelector('.modal'));
  setTimeout(() => {
    instance.open();
  }, 20000);

  setTimeout(() => { info.classList.add('zoomOut'); }, 2000);
  setTimeout(() => { info.remove(); }, 3000);

  document.getElementById('sendData').addEventListener('click', sendData);
});
