const options = document.querySelectorAll('li');

const removeCurrentClass = () => {
  options.forEach(el => {
    el.classList.remove('current');
  });
};

const getLink = (ev) => {
  ev.preventDefault();
  const li = ev.target;
  location.href = li.getAttribute('data-url');
};

options.forEach(el => {
  el.addEventListener('click', getLink);
});


console.log(window.innerWidth);