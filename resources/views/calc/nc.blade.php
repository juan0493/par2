<div class="calc-js">
  <div class="calc-component">
    <hr>
    <div class="top">
      <p class="calc-title">GANAS</p>
      <p class="calc-total">C$0.00</p>
      <div class="number">
        <span><sup>C$</sup>0</span><img src="{{ asset('img/v2/price/dinero_wow.jpg') }}" alt="Dinero Wow">
      </div>
    </div>
    <div class="cont-butons">
      <span class="hide">nc</span>
      <table class="browser-default">
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/nc/1.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/nc/2.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/nc/3.png') }}" alt="Button"></td>
          <td rowspan="3"><img class="pl" src="{{ asset('img/v2/price/plus.png') }}" alt="Button"></td>
        </tr>
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/nc/4.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/nc/5.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/nc/6.png') }}" alt="Button"></td>
        </tr>
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/nc/7.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/nc/8.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/nc/9.png') }}" alt="Button"></td>
        </tr>
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/nc/10.png') }}" alt="Button"></td>
          <td><img class="cl" src="{{ asset('img/v2/price/clean.png') }}" alt="Button"></td>
          <td colspan="2"><img class="eq" src="{{ asset('img/v2/price/equal.png') }}" alt="Button"></td>
        </tr>
      </table>
    </div>
  </div>
  <p class="text-calc">
    <span>¿CÓMO USAR LA CALCULADORA?</span>
    ¡Es muy fácil! si quiere comprar un calzado de C$199 y también quieres uno de C$699 Solo debes elegir el monto y sumarlo con el otro monto, al darle = ¡Sabrás cuando Dinero Wow! ganas!
  </p>
</div>