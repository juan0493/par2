<div class="calc-js">
  <div class="calc-component">
    <hr>
    <div class="top">
      <p class="calc-title">GANAS</p>
      <p class="calc-total">$0.00</p>
      <div class="number">
        <span><sup>$</sup>0</span><img src="{{ asset('img/v2/price/dinero_wow.jpg') }}" alt="Dinero Wow">
      </div>
    </div>
    <div class="cont-butons">
      <span class="hide">sv</span>
      <table class="browser-default">
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/sv/1.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/sv/2.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/sv/3.png') }}" alt="Button"></td>
          <td rowspan="3"><img class="pl" src="{{ asset('img/v2/price/plus.png') }}" alt="Button"></td>
        </tr>
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/sv/4.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/sv/5.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/sv/6.png') }}" alt="Button"></td>
        </tr>
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/sv/7.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/sv/8.png') }}" alt="Button"></td>
          <td><img class="n" src="{{ asset('img/v2/price/sv/9.png') }}" alt="Button"></td>
        </tr>
        <tr>
          <td><img class="n" src="{{ asset('img/v2/price/sv/10.png') }}" alt="Button"></td>
          <td><img class="cl" src="{{ asset('img/v2/price/clean.png') }}" alt="Button"></td>
          <td colspan="2"><img class="eq" src="{{ asset('img/v2/price/equal.png') }}" alt="Button"></td>
        </tr>
      </table>
    </div>
  </div>
  <p class="text-calc">
    <span>¿CÓMO USAR LA CALCULADORA?</span>
    ¡Es muy fácil! si quiere comprar un calzado de $5.99 y también quieres uno de $14.99. Solo debes elegir el monto y sumarlo con el otro monto, al darle = ¡Sabrás cuando Dinero Wow! ganas!
  </p>
</div>