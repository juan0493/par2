@extends('layout.main')

@section('title', 'Catálogo Par2 - '.$country)

@section('extraCSS')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" integrity="sha256-HtCCUh9Hkh//8U1OwcbD8epVEUdBvuI8wj1KtqMhNkI=" crossorigin="anonymous" />
@endsection

@section('content')
  <section id="info" class="animated">
    <img src="{{ asset('img/book.png') }}" alt="Flip book">
  </section>
  <div class="header-flip">
    <a class="lHome" href="{{ url('/') }}">
      <img src="{{ asset('img/logo.png') }}" alt="Par 2">
    </a>    
    <a class="waves-effect waves-light btn modal-trigger green accent-4" href="#modalSub">¡Suscríbete!</a>    
  </div>
  
  <iframe class="container" src="{{ $link }}" frameborder="0"></iframe>

  <!-- Modal Structure -->
  <div id="modalSub" class="modal">
    <div class="modal-content">
      <span class="modal-close">X</span>
      <h5>Suscríbete a nuestro newsletter y serás el primero en enterarte de: nuestras colecciones, eventos y promociones</h5>
      <div class="form">
        <div class="row">
          <div class="col s12 m6 l6 xl6">
            <label>Nombre:</label>
            <input id="fname" type="text" />
          </div>
          <div class="col s12 m6 l6 xl6">
            <label>Apellido:</label>
            <input id="lname" type="text" />
          </div>
          <div class="col s12 m6 l6 xl6">
            <label>Teléfono:</label>
            <input id="phone" type="text" />
          </div>
          <div class="col s12 m6 l6 xl6">
            <label>Correo Electrónico:</label>
            <input id="email" type="text" />
          </div>
        </div>        
        <a id="sendData" href="#">SUSCRIBIRSE</a>
      </div>
    </div>
  </div>
@endsection

@section('extraJS')
  <script src="{{ asset('js/app.js') }}?now={{ time() }}"></script>
@endsection
