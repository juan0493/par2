@extends('layout.main')

@section('title', 'Catálogo Par2 - '.$country)

@section('extraCSS')
<style>
  .header-flip {
    margin-bottom: 2rem !important;
  }
  footer {
    margin-top: 2rem !important;
  }
</style>
@endsection

@section('content')
  <div class="header-flip">
    <a class="lHome" href="{{ url('/') }}">
      <img src="{{ asset('img/logo.png') }}" alt="Par 2">
    </a>
  </div>  
  
  <iframe class="container" src="http://online.fliphtml5.com/atleb/ugoc/#p=1" frameborder="0"></iframe>
  
@endsection

