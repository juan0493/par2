@extends('layout.main')

@section('title', 'Catálogo Par2 - '.$country)

@section('extraCSS')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" integrity="sha256-z78IDnzMlkZK8v2CTwM19nwgmpqbCSYiRtBGqX9bmsA=" crossorigin="anonymous" />
@endsection

@section('content')

  <div class="coin-bg {{ $comp }}">
    <header>
      <div class="logo">
        <a href="{{ url('/') }}"><img src="{{ asset('img/v2/logo.png') }}" alt="Par 2"></a>
      </div>
      <div class="container right-align">
        <a class="waves-effect waves-light btn modal-trigger yellow darken-4" href="#modalSub">SUSCRIBIRSE</a>    
      </div>
    </header>
    <section>
      <div class="cont-swiper">
        <div class="swiper-container z-depth-3">
          <div class="swiper-wrapper">
            @for ($i = 1; $i <= $banner[$comp]; $i++)
              <div class="swiper-slide"><img src="{{ asset('img/v2/banner/'.$comp.'/'.$i.'.png') }}" alt="Promocion"></div>
            @endfor
          </div>
          <div class="swiper-pagination"></div>
          <!-- Add Arrows -->
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
      </div>
    </section>
    <section class="list">
      <ul>
        <li id="li-game">
          <img src="{{ asset('img/v2/icons/game.png') }}" alt="Juega y Gana">
          <span>JUEGA Y GANA</span>
        </li>
        <li id="li-catalogue">
          <img src="{{ asset('img/v2/icons/catalogo.png') }}" alt="Catálogo">
          <span>CATÁLOGO</span>
        </li>
        <li id="li-promo">
          <img src="{{ asset('img/v2/icons/promo.png') }}" alt="Promociones">
          <span>PROMOCIONES</span>
        </li>
        <li id="li-store">
          <img src="{{ asset('img/v2/icons/tienda.png') }}" alt="Tienda">
          <span>ENCUENTRA<br>TU TIENDA</span>
        </li>
      </ul>
    </section>
    <section class="video-full container">
      <div class="video-container">
        <iframe 
          scrolling="no" 
          src="https://www.youtube.com/embed/17u7hZKpSK0?rel=0&amp;controls=0&amp;showinfo=0" 
          frameborder="0" 
          allow="autoplay; encrypted-media" 
          allowfullscreen
        ></iframe>
      </div>
    </section>
    <div class="aux-gradient"></div>
  </div>

  <section id="leaderboard-cont" class="center-align bg-white">
    <h2>JUEGA Y GANA</h2>
    <div id="leaderboard">
      <div class="logo">
        <img src="{{ asset('img/v2/game/assets/logo_peque.png') }}" alt="Par 2">
      </div><!--
      --><div class="table-positions">
        <h3>Tabla de Líderes</h3>

        @if (count($leader) <= 0)
          <p style="width: 100%; text-align: center;">¡Tu puedes ser el primero en el top!</p>
        @else
          <ul class="positions">
            @foreach ($leader as $item)
              <li>{{ $loop->iteration }}</li>
            @endforeach
          </ul>
          <ul class="names">
            @foreach ($leader as $item)
              <li>
                <span class="hide-score position">{{ $loop->iteration }}</span>
                <span class="show">{{ $item->name }}</span>
                <span class="hide-score">{{ $item->name }}: {{ number_format($item->score) }} pts</span>
              </li>
            @endforeach
          </ul>
          <ul class="scores">
            @foreach ($leader as $item)
              <li>{{ number_format($item->score) }} pts</li>
            @endforeach
          </ul>
        @endif
      </div>
    </div>
    <div class="container">
      <a href="{{ setting('site.game_link') }}" target="_blank" title="¡Descarga y gana!">
        <img class="responsive-img" src="{{ asset('img/v2/game/download.png') }}" alt="Descargar">
      </a>
    </div>
  </section>

  {{--
  <section id="calc" class="calc bg-aqua {{ $comp }}">
    <h1>¡GANA DINERO WOW!</h1>
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l6 xl6">
          <h2>Calcula cuanto Dinero WOW!<br>acumulas por tus compras</h2>
          <iframe 
            scrolling="no" 
            src="https://www.youtube.com/embed/Rao290LxZIM?rel=0&amp;controls=0&amp;showinfo=0" 
            frameborder="0" 
            allow="autoplay; encrypted-media" 
            allowfullscreen
          ></iframe>
        </div>
        <div class="col s12 m12 l6 xl6">
          @component('calc.'.$comp)
          @endcomponent
        </div>
      </div>
    </div>
  </section>
  --}}

  <section id="catalogo" class="bg-white">
    <h2>CATÁLOGO</h2>
    <iframe 
      class="container"
      src="{{ $link }}"
      seamless="seamless"
      frameborder="0" 
      allowtransparency="true" 
      allowfullscreen="true" 
    ></iframe>
  </section>

  <section id="promociones" class="bg-white">
    <h2>PROMOCIONES</h2>
    <div class="row">
      <div style="max-width: 1700px; margin: 0 auto;"><img data-caption="¡Super promociones en Par2!" src="{{ asset('img/v2/promo/'.$comp.'/1.png') }}" alt="Promocion 1"></div>
      @if (isset($promo2))
        <div class="col s12 m6"><img data-caption="¡Descuentos en comida por tus compras!" src="{{ asset('img/v2/promo/'.$comp.'/2.png') }}" alt="Promocion 2"></div>
      @endif
      @if (isset($promo3))          
        <div class="col s12 m6"><img data-caption="¡Descuentos en tu calzado favorito!" src="{{ asset('img/v2/promo/'.$comp.'/3.png') }}" alt="Promocion 3"></div>
      @endif
    </div>
  </section>

  <section id="store" class="bg-white container">    
    <h2 class="p">ENCUENTRA TU TIENDA<span>MÁS CERCANA</span></h2>
    <p class="flow-text center-align">Seleccione los campos para encontrar tu Tienda Par2 más cercana</p>

    <div class="row">
      <div class="input-field col s12 m6">
        <select id="selectDpto">
          <option value="" disabled selected>Selecciona departamento</option>
          @foreach ($dbDepartment as $department)
            <option value="{{$department->id}}">{{$department->name}}</option>
          @endforeach
        </select>
        <label>Departamento</label>
      </div>
      <div class="input-field col s12 m6">
        <select id="selectCity" disabled>
          <option value="" disabled selected>Selecciona municipio</option>
        </select>
        <label>Municipio</label>
      </div>
      <div class="col s12">
        <div class="progress grey lighten-5 op">
          <div class="indeterminate amber darken-3"></div>
        </div>
      </div>
    </div>  
    
    <div id="result-stores" class="row"></div>

  </section>

  <!-- Modal Structure -->
  <div id="modalSub" class="modal">
    <div class="modal-content">
      <span class="modal-close">X</span>
      <h5>Suscríbete a nuestro newsletter y serás el primero en enterarte de: nuestras colecciones, eventos y promociones</h5>
      <div class="form">
        <div class="row">
          <div class="col s12 m6 l6 xl6">
            <label>Nombre:</label>
            <input id="fname" type="text" />
          </div>
          <div class="col s12 m6 l6 xl6">
            <label>Apellido:</label>
            <input id="lname" type="text" />
          </div>
          <div class="col s12 m6 l6 xl6">
            <label>Teléfono:</label>
            <input id="phone" type="text" />
          </div>
          <div class="col s12 m6 l6 xl6">
            <label>Correo Electrónico:</label>
            <input id="email" type="text" />
          </div>
        </div>        
        <a id="sendData" href="#">SUSCRIBIRSE</a>
      </div>
    </div>
  </div>

  <div class="fixed-action-btn">
    <a id="go-home" class="hide waves-effect waves-light btn-floating btn-large grey darken-4">
      <i class="large material-icons">arrow_upward</i>
    </a>
  </div>
@endsection

@section('extraJS')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/noframework.waypoints.min.js" integrity="sha256-XJuslujM3cGzRZGiSi/KNSdk58uORO/mmrEQNjVtb5k=" crossorigin="anonymous"></script>
  <script>
    localStorage.setItem('currentCountry', '{{$comp}}');
  </script>
  <script src="{{ asset('js/app.js') }}?now={{ time() }}"></script>
@endsection
