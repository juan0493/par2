<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/apple-icon-57x57.png') }}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/apple-icon-60x60.png') }}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/apple-icon-72x72.png') }}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon-76x76.png') }}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/apple-icon-114x114.png') }}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/apple-icon-120x120.png') }}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/apple-icon-144x144.png') }}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/apple-icon-152x152.png') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-icon-180x180.png') }}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('img/android-icon-192x192.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicon-96x96.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
  <link rel="manifest" href="{{ asset('img/manifest.json') }}">
  <meta name="msapplication-TileColor" content="#f99135">
  <meta name="msapplication-TileImage" content="{{ asset('img/ms-icon-144x144.png') }}">
  <meta name="theme-color" content="#f99135">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
  <title>Access</title>
  <style>
    body {
      align-items: center;
      display: flex;
      height: 100vh;
      justify-content: center;      
    }
    section {
      max-width: 400px;
      padding-top: 20px;
      width: 100%;
    }
  </style>
</head>
<body>
  <section>    
    <div class="row">
      <form class="col s12" method="POST" action="{{ url('/preview/temporal-view/subs') }}">
        @csrf
        @if (isset($msg))          
          <h5>
            {{ $msg }}
          </h5>
        @endif
        <div class="row">
          <div class="col s12">
            <div class="input-field">
              <input name="clave" id="clave" type="password" autocomplete="off" class="validate" required>
              <label for="clave">Pass</label>
              <span class="helper-text">Clave de acceso para ver listado</span>
            </div>
            <button class="waves-effect waves-light btn" type="submit">
              <i class="material-icons right">send</i> Enviar
            </button>
          </div>
        </div>        
      </form>
    </div>  
  </section>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>    
</body>
</html>