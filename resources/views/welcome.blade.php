@extends('layout.main')

@section('title', 'Par2 - Siempre a tu alcance')

@section('content')
  <div class="options">
    <img src="{{ asset('img/logo.png') }}" alt="Par 2">
    <p>¿desde dónde nos visitas?</p>
    <ul>
      <li data-url="{{ url('/catalogo/sv') }}">el salvador</li>
      <li data-url="{{ url('/catalogo/gt') }}">guatemala</li>
      <li data-url="{{ url('/catalogo/cr') }}">costa rica</li>
      <li data-url="{{ url('/catalogo/nc') }}">nicaragua</li>
      <li data-url="{{ url('/catalogo/hn') }}">honduras</li>
    </ul>    
  </div>
@endsection

@section('extraJS')
  <script src="{{ asset('js/home.js') }}"></script>
@endsection