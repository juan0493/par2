<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('score/save', 'ScoresController@save');
Route::post('score/reset', 'ScoresController@reset');
Route::get('score/positions', 'ScoresController@positions');

Route::post('score/register', 'ScoresController@register');
Route::post('score/check', 'ScoresController@check');
Route::post('score/remove', 'ScoresController@remove');


Route::post('dev/score/save', 'DevScoresController@save');
Route::post('dev/score/reset', 'DevScoresController@reset');
Route::get('dev/score/positions', 'DevScoresController@positions');

Route::post('dev/score/register', 'DevScoresController@register');
Route::post('dev/score/check', 'DevScoresController@check');
Route::post('dev/score/remove', 'DevScoresController@remove');
Route::get('dev/score/all', 'DevScoresController@all');


Route::get('getTowns/{dartmentId}', 'DataController@getTowns');
Route::get('getStores/{dartmentId}', 'DataController@getStores');