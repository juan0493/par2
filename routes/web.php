<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('catalogo/{country}', 'FlipbookController@index');
Route::post('saveUser', 'FlipbookController@saveUser');

Route::get('preview/temporal-view/subs', function() { return view('tmp-home'); });
Route::post('preview/temporal-view/subs', 'FlipbookController@subs');
Route::get('preview/temporal-view/subs/export', 'ExportUsersController@export');



Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
